from numpy import loadtxt
import gym
import tensorflow as tf
import numpy as np
import keras
import random
import datetime
from collections import deque
from tensorflow.python.keras.callbacks import TensorBoard
from time import time
import matplotlib.pyplot as plt


def dqn_agent(observation_shape, action_shape):
    init = tf.keras.initializers.he_uniform
    model = tf.keras.Sequential(
        [
            tf.keras.layers.Dense(24, input_shape=observation_shape, activation="relu", name="layer1",
                                  kernel_initializer=init),
            tf.keras.layers.Dense(12, activation="relu", name="layer2", kernel_initializer=init),
            tf.keras.layers.Dense(action_shape, activation="linear", name="layer3", kernel_initializer=init)
        ]
    )
    model.compile(tf.optimizers.Adam(lr=0.001), loss='huber')

    return model


def get_qs(model, state):
    return model.predict(state.reshape([1, state.shape[0]]))


def train(model, memory):
    if len(memory) < 1000:
        return

    alpha = 0.0001
    gamma = 0.99
    batch_size = 128
    train_batch = random.sample(memory, batch_size)
    # train_batch.append(memory[-1])
    state_list = np.array([transition[1] for transition in train_batch])
    q_list = model.predict(state_list)
    new_state_list = np.array([transition[0] for transition in train_batch])
    new_q_list = model.predict(new_state_list)
    X = []
    Y = []
    for index, (new_observation, observation, reward, done, action) in enumerate(train_batch):
        if not done:
            max_future_reward = reward + gamma * np.max(new_q_list[index])
        else:
            max_future_reward = reward

        q_list[index][action] = (1 - alpha) * q_list[index][action] + alpha * max_future_reward
        X.append(state_list[index])
        Y.append(q_list[index])

    model.fit(np.array(X), np.array(Y), verbose=0, batch_size=batch_size, shuffle=True)


env = gym.make('Acrobot-v1')

agent = dqn_agent(env.observation_space.shape, env.action_space.n)
# agent.load_weights('QNN.csv')
target_agent = agent

max_epsilon = 1
epsilon = max_epsilon
min_epsilon = 0.1
episodes = 400
reduction_rate = 0.01
memory_replay = deque(maxlen=25000)
train_index = 0
Reward_vector = []

for i_episode in range(episodes):
    observation = env.reset()
    total_reward = 0
    train_index += 1
    done = False

    if epsilon > min_epsilon:
        epsilon -= reduction_rate
    while not done:

        env.render()

        if random.uniform(0, 1) < epsilon:
            action = env.action_space.sample()
        else:
            action = np.argmax(get_qs(target_agent, observation))

        new_observation, reward, done, info = env.step(action)
        memory_replay.append([new_observation, observation, reward, done, action])
        observation = new_observation

        if train_index % 4 == 0 or done:
            train(agent, memory_replay)

        total_reward += reward

        if done:
            if train_index >= 100:
                print('Copying main network weights to the target network weights')
                target_agent.set_weights(agent.get_weights())
                train_index = 0

            # print("Episode finished after {} timesteps".format(t + 1))
            Reward_vector.append(total_reward)
            print(total_reward)
            print("Episode {}".format(i_episode))
            break

N = 100
cumsum, moving_aves = [0], []

for i, x in enumerate(Reward_vector, 1):
    cumsum.append(cumsum[i - 1] + x)
    if i >= N:
        moving_ave = (cumsum[i] - cumsum[i - N]) / N
        # can do stuff with moving_ave here
        moving_aves.append(moving_ave)

plt.plot(moving_aves)
plt.show()
agent.save_weights('acrobot.csv')
