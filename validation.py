import tensorflow as tf
import gym
import numpy as np

def dqn_agent(observation_shape, action_shape):
    init = tf.keras.initializers.he_uniform
    model = tf.keras.Sequential(
        [
            tf.keras.layers.Dense(24, input_shape=(4,), activation="relu", name="layer1", kernel_initializer=init),
            tf.keras.layers.Dense(12, activation="relu", name="layer2", kernel_initializer=init),
            tf.keras.layers.Dense(2, activation="linear", name="layer3", kernel_initializer=init)
        ]
    )
    model.compile(tf.optimizers.Adam(lr=0.001), loss='huber')

    return model


def get_qs(model, state):
    return model.predict(state.reshape([1, 4]))

env = gym.make('CartPole-v0')

agent = dqn_agent(env.observation_space.shape, 2)
agent.load_weights('QNN.csv')

episodes = 100

for i_episode in range(episodes):
    observation = env.reset()
    total_reward = 0
    done = False

    while not done:
        env.render()
        action = np.argmax(get_qs(agent, observation))
        observation, reward, done, info = env.step(action)
