# DeepQLearning

## dqn.py trains the agent.
## validation.py runs the trained agent.

Each OpenAI Gym game may require a slightly different strategy.
For example CartPole-v0 requires a target network which is updated once in a while in order to improve stability.
Acrobot-v1 in contrast can afford to be updated online. In fact, this is useful as each episode takes significantly more time than in CartPole.

